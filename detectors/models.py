from django.db import models


# Create your models here.
class DetectHistory(models.Model):
    index = models.CharField(max_length=32)
    image_path = models.CharField(max_length=256)
    detect_results = models.JSONField()
    req_timestamp = models.BigIntegerField()
    res_timestamp = models.BigIntegerField()
