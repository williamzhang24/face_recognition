import time

import cv2
from django.shortcuts import render
from django.views import View
from detectors import models
import json
from django.http import HttpResponse, StreamingHttpResponse
import os
import base64
import numpy as np
import global_vars
import face_recognition

# Create your views here.

# [IN] RGB, [OUT] RGB
def base64_to_img(base64_str):
    byte_data = base64.b64decode(base64_str)  # 将base64转换为二进制
    encode_image = np.asarray(bytearray(byte_data), dtype='uint8')  # 二进制转换为一维数组
    img_array = cv2.imdecode(encode_image, cv2.IMREAD_COLOR)  # 用cv2解码为三通道矩阵 BGR
    return cv2.cvtColor(img_array, cv2.COLOR_BGR2RGB)


# [IN] RGB, [OUT] RGB
def img_to_base64(img_array_rgb):
    img_array_bgr = cv2.cvtColor(img_array_rgb, cv2.COLOR_RGB2BGR)  # BGR
    encode_image = cv2.imencode('.jpg', img_array_bgr)[1]  # 用cv2压缩/编码，转为一维数组, RGB
    byte_data = encode_image.tobytes()  # 转换为二进制
    base64_str = base64.b64encode(byte_data).decode("ascii")  # 转换为base64
    return base64_str

def detect_face(frame):
    # Resize frame of video to 1/4 size for faster face recognition processing
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
  
    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_small_frame = small_frame[:, :, ::-1]
  
    # Find all the faces and face encodings in the current frame of video
    face_locations = face_recognition.face_locations(rgb_small_frame)
    face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)
  
    face_names = []
    for face_encoding in face_encodings:
        # See if the face is a match for the known face(s)
        matches = face_recognition.compare_faces(global_vars.get_value('g_known_face_encodings'), face_encoding)
        name = "Unknown"
  
        # # If a match was found in known_face_encodings, just use the first one.
        # if True in matches:
        #     first_match_index = matches.index(True)
        #     name = known_face_names[first_match_index]
  
        # Or instead, use the known face with the smallest distance to the new face
        face_distances = face_recognition.face_distance(global_vars.get_value('g_known_face_encodings'), face_encoding)
        best_match_index = np.argmin(face_distances)
        if matches[best_match_index]:
            name = global_vars.get_value('g_known_face_names')[best_match_index]
  
        face_names.append(name)
        
    face_locations = np.array(face_locations) * 4
        
    return face_locations.tolist(), face_names
           
class DetectView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.g_req_imgs_save_path = global_vars.get_value('g_req_imgs_save_path')

    def post(self, request):
        req_timestamp = time.time_ns()
        
        try:
            req_json = json.loads(request.body)
            img_b64 = req_json['img_b64']  # RGB

            img_rgb = base64_to_img(img_b64)  # RGB
            face_locations, face_names = detect_face(img_rgb)

            det_res_json = {'locations': face_locations, 'names': face_names}
            det_res_json_str = json.dumps(det_res_json)
            
            if len(face_names) > 0 :
              req_idx = time.time_ns()
              img_files = sorted(os.listdir(self.g_req_imgs_save_path))
              if len(img_files) >= 1000:
                os.remove(os.path.join(self.g_req_imgs_save_path, img_files[0])) 
              image_path = os.path.join(self.g_req_imgs_save_path, f'{req_idx}.jpg')
              cv2.imwrite(image_path, cv2.cvtColor(img_rgb, cv2.COLOR_RGB2BGR))
              models.DetectHistory.objects.create(
                  index=req_idx,
                  image_path=image_path,
                  detect_results=det_res_json,
                  req_timestamp=req_timestamp,
                  res_timestamp=time.time_ns()
              )

            return HttpResponse(det_res_json_str)
        except Exception as e:
            print(f'Exception: {e}')
            return HttpResponse(json.dumps({'status': False, 'msg': u'Error: {}'.format(e)}))
            
            
class VisualizeView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
    
    def gen(self):
      while True:
        last_req = models.DetectHistory.objects.order_by('req_timestamp').last()
        detect_results = last_req.detect_results
        orig_img_bgr = cv2.imread(last_req.image_path)
        for i in range(len(detect_results['locations'])):
          top, right, bottom, left = detect_results['locations'][i]
          # Draw a box around the face
          cv2.rectangle(orig_img_bgr, (left, top), (right, bottom), (0, 0, 255), 2)
  
          # Draw a label with a name below the face
          cv2.rectangle(orig_img_bgr, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
          font = cv2.FONT_HERSHEY_DUPLEX
          cv2.putText(orig_img_bgr, detect_results['names'][i], (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
  
        orig_img_bgr = cv2.resize(orig_img_bgr, (0, 0), fx=0.5, fy=0.5)
        ret, frame = cv2.imencode('.jpeg', orig_img_bgr)
        if ret:
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame.tobytes() + b'\r\n')
        
    def get(self, request):
      return StreamingHttpResponse(self.gen(), content_type='multipart/x-mixed-replace; boundary=frame')
        
        
        
        
        
