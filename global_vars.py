import os
import face_recognition

def init():
    global _global_dict
    _global_dict = {}
    set_value('g_root_path', os.getcwd())
    set_value('g_req_imgs_save_path', os.path.join(get_value('g_root_path'), 'req_imgs'))
    
    zhangweiyi_image = face_recognition.load_image_file(os.path.join(get_value('g_root_path'), 'zhangweiyi.jpg'))
    zhangweiyi_face_encoding = face_recognition.face_encodings(zhangweiyi_image)[0]
    known_face_encodings = [zhangweiyi_face_encoding]
    known_face_names = ["Zhangweiyi"]
    # Create arrays of known face encodings and their names
    set_value('g_known_face_encodings',known_face_encodings)
    set_value('g_known_face_names',known_face_names)
    


def set_value(name, value):
    _global_dict[name] = value


def get_value(name, default_value=None):
    try:
        return _global_dict[name]
    except KeyError:
        return default_value
